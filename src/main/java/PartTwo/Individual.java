package PartTwo;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by Yusuf on 16-6-2016.
 */
public class Individual {
    private String[] chromosome;
    double fitness = 0;

    public String[] getChromosome() {
        return chromosome;
    }

    public void setChromosome(String[] chromosome) {
        this.chromosome = chromosome;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    @Override
    public String toString() {
        return Arrays.toString(chromosome) + " - " + fitness;
    }

}