package PartTwo;

import javafx.util.Pair;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Yusuf on 16-6-2016.
 */
public class App {

    Random randomValue = new Random();
    String target = Integer.toBinaryString(5);

    public Individual createIndividual(){
        int size = 10;
        int range = 32;
        Individual individual = new Individual();
        String[] chromosome = new String[size];

        for(int i = 0; i < size; i++){
            chromosome[i] = Integer.toBinaryString(new Random().nextInt(range)) ;
        }
        individual.setChromosome(chromosome);

        return individual;
    }

    public double computeFitness(String[] chromosome){
        double highestFitness = 0;

        for(int i = 0; i < chromosome.length; i++){
            int number = Integer.parseInt(chromosome[i],2);
            double fitness  = -(Math.pow(number,2)) + 7 * number;
            if(fitness > highestFitness){
                highestFitness = fitness;
            }
        }

        return highestFitness;
    }

    public Map<Individual, Individual> selectTwoParents(Individual[] population){
        Map<Individual, Individual> parents = new HashMap<>();

        double totalFitness = IntStream.range(0,population.length +1).sum();

        Arrays.sort(population, Comparator.comparing(Individual::getFitness));
        for(int i = 0; i < population.length; i++){
            double fitnessRanking = (i+1) / totalFitness;
            population[i].setFitness(fitnessRanking);
        }

        double random = Math.random();
        double cumulativeFitness = 0;
        for(int j = 0; j < population.length; j++){
            cumulativeFitness += population[j].getFitness();
            if(cumulativeFitness > random){
                parents.put(population[j], null);
                break;
            }
        }

        parents.entrySet().forEach(individualEntry -> {
            while (individualEntry.getValue() == null) {
                double randomValue = Math.random();
                double cumulativeFitnessValue = 0;
                for (int j = 0; j < population.length; j++) {
                    cumulativeFitnessValue += population[j].getFitness();
                    if (cumulativeFitnessValue > randomValue && individualEntry.getKey() != population[j]) {
                        parents.put(individualEntry.getKey(), population[j]);
                    }
                }
            }
        });
        return parents;
    }

    public Map<Individual, Individual> crossover(Map<Individual, Individual> parents){
        Map<Individual, Individual> children = new HashMap<>();
        parents.entrySet().forEach(mapEntry -> {
            String[] parentOnePartOne = Arrays.copyOfRange(mapEntry.getKey().getChromosome(), 0, 5);
            String[] parentOnePartTwo = Arrays.copyOfRange(mapEntry.getKey().getChromosome(), 5, 10);

            String[] parentTwoPartOne = Arrays.copyOfRange(mapEntry.getValue().getChromosome(), 0, 5);
            String[] parentTwoPartTwo = Arrays.copyOfRange(mapEntry.getValue().getChromosome(), 5, 10);

            String[] chromosomeOne = Stream.concat(Stream.of(parentOnePartOne), Stream.of(parentTwoPartTwo)).toArray(String[]::new);
            String[] chromosomeTwo = Stream.concat(Stream.of(parentTwoPartOne), Stream.of(parentOnePartTwo)).toArray(String[]::new);

            Individual childOne = new Individual();
            childOne.setChromosome(chromosomeOne);

            Individual childTwo = new Individual();
            childTwo.setChromosome(chromosomeTwo);

            children.put(childOne, childTwo);
        });
        return children;
    }

    public Individual mutation(Individual individual, double mutationRate){
        if(mutationRate < Math.random()){
            individual.getChromosome()[0] = Integer.toBinaryString(new Random().nextInt(32));
        }
        return individual;
    }

    public void run(int populationSize, int numIterations, boolean elitism, double crossoverRate, double mutationRate){
        Individual[] initialPopulation = new Individual[populationSize];
        Individual[] currentPopulation = initialPopulation;
        for(int generation = 0; generation < numIterations; generation++){
            Individual[] nextPopulation = new Individual[populationSize];
            for(int i = 0; i < populationSize; i++){
                Individual individual = createIndividual();
                double fitness = computeFitness(individual.getChromosome());
                individual.setFitness(fitness);
                currentPopulation[i] = individual;
            }

            int startIndex;
            if(elitism){
                startIndex = 1;
                Arrays.sort(currentPopulation,Comparator.comparing(Individual::getFitness));
                Individual bestIndividual = currentPopulation[populationSize-1];
                nextPopulation[0] = bestIndividual;
            }else {
                startIndex = 0;
            }
            for(int newInd = startIndex; newInd < populationSize; newInd++){
                Map<Individual, Individual> parents = selectTwoParents(currentPopulation);
                Map<Individual, Individual> offspring;
                double random = randomValue.nextDouble();
                if(random  < crossoverRate){
                    offspring = crossover(parents);
                } else {
                    offspring = parents;
                }
                nextPopulation[newInd++] = mutation(offspring.entrySet().stream().findFirst().get().getKey(),mutationRate);
                if(newInd < populationSize){
                    nextPopulation[newInd] = mutation(offspring.entrySet().stream().findFirst().get().getValue(),mutationRate);
                }
            }
            currentPopulation = nextPopulation;
        }
        double totalFitness = 0;

        // Recompute fitnesses
        for(int i = 0; i < populationSize; i++){
            double fitness = computeFitness(currentPopulation[i].getChromosome());
            totalFitness += fitness;
            currentPopulation[i].setFitness(fitness);
        }

        Arrays.sort(currentPopulation, Comparator.comparing(Individual::getFitness));
        Individual bestIndividual = currentPopulation[populationSize-1];
        System.out.println("Best Fitness " + bestIndividual.getFitness());
        //System.out.println("Best individual " + bestIndividual );
        System.out.println("Average fitness " + totalFitness / populationSize);
    }

    public static void main(String[] args){
        App app = new App();
        app.run(10,50, false,0.85,0.01);
    }
}