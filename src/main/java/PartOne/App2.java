package PartOne;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

/**
 * Created by Yusuf on 4-7-2016.
 */
public class App2 {
    List<Point> data = new ArrayList<>();
    Map<Integer, Point> data2 = new HashMap<>();

    public void readData() throws IOException {
        String fileName = "C:\\Users\\Y.A\\IdeaProjects\\INFDTA2\\src\\main\\java\\PartOne\\a2.csv";
        File file = new File(fileName);
        List<String> lines = Files.readAllLines(file.toPath());

        for(int i = 0; i < lines.size(); i++){
            String line = lines.get(i);
            String[] stringArray = line.split(",");

            Double x = Double.parseDouble(stringArray[1]);
            Double y = Double.parseDouble(stringArray[2]);

            Point point = new Point(i);
            point.addItem(x);
            point.addItem(y);
            data2.put(i, point);
        }
        System.out.println("Size " + data2.size());
    }

    public Map<Integer, Cluster> setupClusters(int amount){
        Map<Integer, Cluster> clusters = new HashMap<>();

        Set<Integer> randomKeys = new HashSet<>();
        while(randomKeys.size() < amount){
            int randomNumber = new Random().nextInt(data2.values().size());
            randomKeys.add(randomNumber);
        }

        randomKeys.forEach(key -> {
            Point centroid = data2.get(key);
            Cluster cluster = new Cluster(key, centroid);
            clusters.put(key, cluster);
        });

        return clusters;
    }

    public Map<Integer, Cluster> assignPoints(Map<Integer, Cluster> clusters){
        for(Point point : data2.values()){
            double shortestDistance = Double.MAX_VALUE;
            int clusterId = 0;
            for(Cluster cluster : clusters.values()){
                if(point.getId() != cluster.getCentroid().getId()){
                    double distance = computeDistance(point.getItems(), cluster.getCentroid().getItems());
                    if(distance < shortestDistance){
                        shortestDistance = distance;
                        point.setDistance(distance);
                        clusterId = cluster.getId();
                    }
                }
            }
            clusters.get(clusterId).addPoint(point);
        }
        return clusters;
    }

    public double computeDistance(List<Double> pointX, List<Double> pointY){

        double sum = 0;
        for(int i = 0; i < 2; i++){
            sum += Math.pow(pointX.get(i) - pointY.get(i),2);
        }
        return Math.sqrt(sum);
    }

    public double sumSquaredError(Map<Integer, Cluster> clusters){
        double SSE = 0;

        for(Cluster cluster : clusters.values()){
            double squaredError = 0;
            for(Point point : cluster.getPoints()){
                squaredError += Math.pow(point.getDistance(),2);
            }
            SSE += squaredError;
        }
        return SSE;
    }

    public void printClusters(Map<Integer, Cluster> clusters){
        clusters.entrySet().stream().forEach(clusterEntry ->{
            System.out.println("Cluster " + clusterEntry.getKey() + " Size " + clusterEntry.getValue().getPoints().size());
            clusterEntry.getValue().topOrders();
        });
    }

    public void run(int iterations, int amountClusters) throws IOException {
        readData();
        Map<Integer, Cluster> bestList = new HashMap<>();
        Map<Integer,Cluster> clusters = setupClusters(amountClusters);
        double lowestSSE = Double.MAX_VALUE;
        for(int i = 0; i < iterations; i++) {
            clusters.entrySet().forEach(entry -> entry.getValue().clearAll());
            clusters =  assignPoints(clusters);
            for(Cluster cluster : clusters.values()){
                cluster.recomputeCentroid();
            }
            double currentSSE =  sumSquaredError(clusters);
            if(currentSSE < lowestSSE){
                lowestSSE = currentSSE;
                bestList = new HashMap<>(clusters);
            }
        }
        printClusters(bestList);
        System.out.println("------------------------------");
        System.out.println("Lowest SSE " + lowestSSE);
    }


    public static void main(String[] args) throws IOException {
        App2 app2 = new App2();
        app2.run(100,4);
    }
}
