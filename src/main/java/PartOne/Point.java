package PartOne;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yusuf on 4-7-2016.
 */
public class Point {
    private List<Double> items;
    private double distance;
    private int id;

    public int getId() {
        return id;
    }

    public List<Double> getItems() {
        return items;
    }

    public void setItems(List<Double> items) {
        this.items = items;
    }

    public void addItem(Double item){
        items.add(item);
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Point(int id){
        this.id = id;
        items = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "ID " + id + " - " + items;
    }
}
