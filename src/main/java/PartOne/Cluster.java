package PartOne;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by Yusuf on 15-6-2016.
 */
public class Cluster {
    private Point centroid;
    private List<Point> points;
    int id;

    public Point getCentroid() {
        return centroid;
    }

    public List<Point> getPoints() {
        return points;
    }

    public int getId() {
        return id;
    }

    public void addPoint(Point point){
        points.add(point);
    }

    public Cluster(int id, Point centroid) {
        this.points = new ArrayList<>();
        this.id = id;
        this.centroid = centroid;
    }

    public void recomputeCentroid(){
        int size = points.size();
        List<Double> newItems = new ArrayList<>();
        for(int i = 0; i < centroid.getItems().size(); i++){
            double sum = 0;
            for(Point point : points){
                sum += point.getItems().get(i);
            }
            double newValue = sum / size;
            newItems.add(newValue);
        }
        centroid.setItems(newItems);
    }

    public void clearAll(){
        points = new ArrayList<>();
    }

    public void topOrders(){
        Map<Integer, Integer> orders = new HashMap<>();
        for(int i = 0; i < 32; i++) {
            int sum = 0;
            for (Point point : points) {
                sum += point.getItems().get(i);
            }
            orders.put(i, sum);

        }
        orders.entrySet().stream()
                .sorted(Map.Entry.<Integer, Integer>comparingByValue().reversed())
                .limit(3)
                .forEach(System.out::println);
    }

    @Override
    public String toString() {
        return "ID " + id + " - " + points.size();
    }
}
