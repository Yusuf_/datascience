package PartThree;

import javafx.util.Pair;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.List;

/**
 * Created by Yusuf on 14-6-2016.
 */
public class DES extends JFrame {
    List<Double> dataPoints;
    List<Double> smoothing;
    List<Double> trending;
    List<Double> forecasting;
    List<Double> errors;
    public DES(){
        super("Example Chart");


        JPanel chartPanel = createDESPanel();

        //Before here calculate smoothing
        add(chartPanel, BorderLayout.CENTER);

        setSize(640, 480);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

    }

    private void readData(String fileName){
        File file = new File(fileName);
        dataPoints = new ArrayList<>();
        java.util.List<String> lines = null;
        try {
            lines = Files.readAllLines(file.toPath());
            for(int i = 0; i < lines.size(); i++){
                String[] array = lines.get(i).split(",");
                    double sales = Double.parseDouble(array[1]);
                    dataPoints.add(sales);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double initialTrend(){
        return dataPoints.get(1) - dataPoints.get(0);
    }

    public void trendAndSmoothing(double alpha, double beta){
        trending = new ArrayList<>(Arrays.asList(null,initialTrend()));
        smoothing = new ArrayList<>(Arrays.asList(null, Double.valueOf(dataPoints.get(1))));

        for(int i = 2; i < dataPoints.size(); i++){
            double smoothValue = alpha * dataPoints.get(i) + (1 - alpha) * (smoothing.get(i-1) + trending.get(i-1));
            smoothing.add(smoothValue);
            double trendValue = beta * (smoothing.get(i) - smoothing.get(i-1)) + (1 - beta) * trending.get(i-1);
            trending.add(trendValue);
        }
    }

    public void forecast(int months){
        forecasting = new ArrayList<>(Arrays.asList(null, null));
        for(int i = 2; i < trending.size() + 1; i++){
            double forecastValue = trending.get(i -1) + smoothing.get(i-1);
            forecasting.add(forecastValue);
        }
        double lastSmoothness = smoothing.get(smoothing.size()-1);
        double lastTrend = trending.get(trending.size()-1);

        for(int i =  0; i < months; i++){
            double forecastValue = lastSmoothness  + ((i + 1) * lastTrend);
            forecasting.add(forecastValue);
        }
    }

    public double sumSquaredError(){
        errors = new ArrayList<>();
        double SSE = 0;
        for(int i = 2; i < dataPoints.size(); i++){
            double x = dataPoints.get(i);
            double y = forecasting.get(i);
            double error = Math.pow(x - y,2);
            errors.add(error);
            SSE += error;
        }

        SSE = Math.sqrt(SSE/(dataPoints.size()-2));
        return SSE;
    }

    public double bestBeta(){
        double bestBeta = 0;
        double bestAlpha = 0;

        List<Double> trendList = null;
        List<Double> smoothList = null;
        List<Double> forecastList = null;
        double lowestSSE = Double.MAX_VALUE;
        for(int j = 0; j < 100; j++){
            double randomAlpha = Math.random();
            for(int i = 0; i < 100; i++){
                double randomBeta = Math.random();
                trendAndSmoothing(randomAlpha, randomBeta);
                forecast(8);
                double sse = sumSquaredError();
                if(sse < lowestSSE){
                    lowestSSE = sse;
                    bestBeta = randomBeta;
                    bestAlpha = randomAlpha;

                    trendList = new ArrayList<>(trending);
                    smoothList = new ArrayList<>(smoothing);
                    forecastList = new ArrayList<>(forecasting);
                }
            }
        }


        System.out.println("Best Beta " + bestBeta);
        System.out.println("Best Alpha " + bestAlpha);
        System.out.println("SSE " + lowestSSE);
        trending = trendList;
        smoothing = smoothList;
        forecasting = forecastList;

        return bestBeta;
    }

    public XYDataset createData(){
        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries swordSeries = new XYSeries("Swords", false, false);
        XYSeries smoothingSeries = new XYSeries("Smoothing", false, false);
        XYSeries forecastSeries = new XYSeries("Forecast", false, false);

        for(int i = 0; i < dataPoints.size(); i++){
            swordSeries.add(i+1, dataPoints.get(i));
        }

        for(int i = 0; i < smoothing.size(); i++){
            smoothingSeries.add(i+1, smoothing.get(i));
        }

        for(int i = 0; i < forecasting.size(); i++){
            forecastSeries.add(i+1, forecasting.get(i));
        }

        dataset.addSeries(swordSeries);
        dataset.addSeries(smoothingSeries);
        dataset.addSeries(forecastSeries);
        return dataset;
    }

    private JPanel createDESPanel(){
        String chartTitle = "Swords Forecasting";
        String xAxisLabel = "Months";
        String yAxisLabel = "Demand";

        boolean showLegend = true;
        boolean createURL = false;
        boolean createTooltip = false;

        String fileName = "C:\\Users\\Y.A\\IdeaProjects\\INFDTA2\\src\\main\\java\\PartThree\\Swordsdata.csv";

        readData(fileName);
        trendAndSmoothing(0.5, 0.5);
        forecast(8);
        double b = bestBeta();

        XYDataset dataset = createData();
        JFreeChart chart = ChartFactory.createXYLineChart(chartTitle,
                xAxisLabel, yAxisLabel, dataset,
                PlotOrientation.VERTICAL, showLegend, createTooltip, createURL);

        // Create an NumberAxis
        NumberAxis xAxis = new NumberAxis(xAxisLabel);

        // Assign it to the chart
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setDomainAxis(xAxis);

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.RED);
        renderer.setSeriesPaint(1, Color.BLUE);
        renderer.setSeriesPaint(2, Color.GREEN);
        plot.setRenderer(renderer);

        return new ChartPanel(chart);
    }

    public static void main(String[] args){
        new DES().setVisible(true);
    }

}
