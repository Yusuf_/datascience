package PartThree;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.List;

/**
 * Created by Yusuf on 13-6-2016.
 */
public class SES extends JFrame {

    public SES(){
        super("Example Chart");


        JPanel chartPanel = createSESPanel();

        //Before here calculate smoothing
        add(chartPanel, BorderLayout.CENTER);

        setSize(640, 480);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

    }

    private List<Integer> readData(String fileName){
        List<Integer> swordsData = new ArrayList<>();
        File file = new File(fileName);

        java.util.List<String> lines = null;
        try {
            lines = Files.readAllLines(file.toPath());

            for (String line : lines) {
                String[] array = line.split(",");
                int x = Integer.parseInt(array[0]);
                int y = Integer.parseInt(array[1]);
                swordsData.add(y);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return swordsData;
    }

    public double initialSmoothing(List<Integer> swords, int limit){
        int sum = 0;
        for(int i = 0; i < limit; i++){
            sum += swords.get(i);
        }
        return (sum / limit);
    }

    public List<Double> smoothing(List<Integer> swords, double alpha){
        List<Double> smoothing = new ArrayList<>();

        double initialSmoothing = initialSmoothing(swords, 12);
        smoothing.add(initialSmoothing);

        for(int i = 1; i < swords.size() + 1; i++){
            double smoothingValue = alpha * swords.get(i-1) + (1 - alpha) * smoothing.get(i-1);
            smoothing.add(smoothingValue);
        }
        return smoothing;
    }

    public List<Double> forecasting(List<Double> smoothingList, int months){
        List<Double> forecast = new ArrayList<>();
        double lastSmoothing = smoothingList.get(smoothingList.size() - 1);
        for(int i = 0; i < months; i++){
            forecast.add(lastSmoothing);
        }
        return forecast;
    }

    public double sumSquaredError(List<Double> smoothingList, List<Integer> dataPoints){
        double sumSquaredError = 0;
        for(int i = 0; i < dataPoints.size(); i++){
            double error = dataPoints.get(i) - smoothingList.get(i);
            sumSquaredError += Math.pow(error,2);
        }
        return Math.sqrt(sumSquaredError/(smoothingList.size()-1));
    }

    private JPanel createSESPanel() {
        String chartTitle = "Swords Forecasting";
        String xAxisLabel = "Months";
        String yAxisLabel = "Demand";

        boolean showLegend = true;
        boolean createURL = false;
        boolean createTooltip = false;

        String fileName = "C:\\Users\\Y.A\\IdeaProjects\\INFDTA2\\src\\main\\java\\PartThree\\SwordsData.csv";

        List<Integer> dataPoints = readData(fileName);
        double smallestAlpha = bestAlpha(dataPoints);
        List<Double> smoothingPoints = smoothing(dataPoints, smallestAlpha);
        List<Double> forecast = forecasting(smoothingPoints, 12);

        XYDataset dataset = createData(dataPoints, smoothingPoints, forecast);

        JFreeChart chart = ChartFactory.createXYLineChart(chartTitle,
                xAxisLabel, yAxisLabel, dataset,
                PlotOrientation.VERTICAL, showLegend, createTooltip, createURL);

        // Create an NumberAxis
        NumberAxis xAxis = new NumberAxis(xAxisLabel);
        xAxis.setTickUnit(new NumberTickUnit(1));

        // Assign it to the chart
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setDomainAxis(xAxis);


        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.RED);
        renderer.setSeriesPaint(1, Color.BLUE);
        renderer.setSeriesPaint(2, Color.GREEN);
        plot.setRenderer(renderer);

        return new ChartPanel(chart);
    };

    public XYDataset createData(List<Integer> swords, List<Double> smoothing, List<Double> forecast){
        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries swordSeries = new XYSeries("Swords", false, false);
        XYSeries smoothingSeries = new XYSeries("Smoothing", false, false);
        XYSeries forecastSeries = new XYSeries("Forecast", false, false);

        for(int i = 0; i < swords.size(); i++){
            swordSeries.add(i+1, swords.get(i));
        }

        for(int i = 0; i < smoothing.size(); i++){
            smoothingSeries.add(i+1, smoothing.get(i));
        }

        for(int i = 0; i < forecast.size(); i++){
            forecastSeries.add(smoothing.size() + i, forecast.get(i));
        }

        dataset.addSeries(swordSeries);
        dataset.addSeries(smoothingSeries);
        dataset.addSeries(forecastSeries);
        return dataset;
    }

    public double  bestAlpha(List<Integer> dataPoints){
        double smallestSSE = Double.MAX_VALUE;
        double alpha = 0;
        for(int i = 0; i < 100; i++){
            double randomAlpha = Math.random();
            List<Double> smoothingList = smoothing(dataPoints,randomAlpha);
            double SSE = sumSquaredError(smoothingList, dataPoints);
            if(SSE < smallestSSE){
                smallestSSE = SSE;
                alpha = randomAlpha;
            }
        }
        System.out.println("Best Alpha " + alpha);
        System.out.println("Error " + smallestSSE);
        return alpha;
    }
    
    public static void main(String[] args) {
        new SES().setVisible(true);
    }
}
